/**
 * @file        Population.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef POPULATION_H
#define POPULATION_H

#include<map>
#include<string>
#include "Cell.h"
#include "Support/Globals.h"
#include "GoL_Rules/RuleOfExistence.h"
#include "GoL_Rules/RuleFactory.h"

using namespace std;

/**
 * @class Population
 * @brief Representation of the complete society of cell existance and interactions.
 * @details The Population constitutes of all current, previous and future generations of cells, both living and dead
as well as those not yet born. By mapping all cells to their respective positions in the simulation world,
Population has the complete knowledge of each cell's whereabouts. Furthermore, the class is responsible for
determining which rules should be required from the RuleFactory, and store the pointer to these as members.

Population's main responsibility during execution is determining which rule to apply for each new generation
and updating the cells to their new states.

 * @test The population should be properly created and updated
 */
class Population
{
private:
    int generation;
    map<Point, Cell> cells;
    RuleOfExistence* evenRuleOfExistence;
    RuleOfExistence* oddRuleOfExistence;

    /**
     * @brief Build cell culture based on randomized starting values.
     */
    void randomizeCellCulture();

    /**
     * @brief Send cells map to FileLoader, which will populate its culture based on file values.
     */
    void buildCellCultureFromFile();

public:
    /**
     * @brief Constructor that creates an empty Population object
     */
    Population() : generation(0), evenRuleOfExistence(nullptr), oddRuleOfExistence(nullptr) {}

    /**
     * @brief Destructor that frees the allocated space
     */
    ~Population();

    /**
     * @brief Initializing cell culture and the concrete rules to be used in simulation
     * @param evenRuleName The name of the rule to be used for even generations
     * @param oddRuleName The name of the rule to be used for odd generations
     * @test If no filename is set, this function should return different population states at different executions
     * @test If oddRuleName is empty, it should be the same as evenRuleName after execution
     */
    void initiatePopulation(string evenRuleName, string oddRuleName = "");

    /**
     * @brief Update the cell population and determine next generational changes based on rules
     * @return The generation incremented by 1
     * @test The right rules for the generation should be executed and the wrong ones shouldn't
     * @test The returned generation number should be incremented by 1
     * @test When the function is executed the cells should have updated their values
     * @test The Rule of existence members should point to the right rules based on their names after execution
     */
    int calculateNewGeneration();

    /**
     * @brief Returns cell by specified key value
     * @param position The cell's position in the two dimensional world
     * @return A reference to the cell
     * @test The right cell should be returned
     */
    Cell& getCellAtPosition(Point position) { return cells.at(position); }

    /**
     * @brief  Gets the total amount of cells in the population, regardless of state
     * @return The total number of cells in the population
     * @test The right number of cells should be returned
     */
    int getTotalCellPopulation() { return cells.size(); }

};

#endif