/**
 * @file        Cell.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef cellH
#define cellH

#include "../../terminal/terminal.h"

/**
 * @brief Data structure holding colors to visualize the state of cells.
 */
struct StateColors {
    COLOR LIVING, // Representing living cell
            DEAD, // Representing dead cell
            OLD,  // Representing old cell
            ELDER;// Representing very old cell
}

/**
 * @brief Initiate default values.
 */
const STATE_COLORS = { COLOR::WHITE, COLOR::BLACK, COLOR::CYAN, COLOR::MAGENTA };

/**
 * @brief Cell action. Determined by rule, and sent to cell for future change.
 */
enum ACTION { KILL_CELL, IGNORE_CELL, GIVE_CELL_LIFE, DO_NOTHING };


/**
 * @class Cell
 * @brief Cells represents a certain combination of row and column of the simulated world.
 * @details Cells may be of two types; rim cells, those representing the outer limits of the world,
or non-rim cells. The first cell type are immutable, exempt from the game's rules, and
thus their values may not be changed. The latter type, however, may be changed and edited
in ways specified by the rules.
 */
class Cell {

private:
    /**
     * @brief A data structure that encapsulates cell details
     */
    struct CellDetails {
        int age;
        COLOR color;
        bool rimCell;
        char value;
    } details;

    /**
     * @brief Data structure that encapsulates changes to next state
     */
    struct NextUpdate {
        ACTION nextGenerationAction;
        COLOR nextColor;
        char nextValue;
        bool willBeAlive;	// some rules may need to know beforehand whether the cell will be alive
    } nextUpdate;

    /**
     * @brief Adds 1 to the cell's age
     */
    void incrementAge() { details.age++; }

    /**
     * @brief Kills the cell and sets age to 0
     */
    void killCell() { details.age = 0; }

    /**
     * @brief Sets the character value of the cell, which will be printed to screen.
     * @param value The character to be printed to screen
     */
    void setCellValue(char value) { details.value = value; }

    /**
     * @brief Sets color of the cell
     * @param color The color the cell will have
     */
    void setColor(COLOR color) { this->details.color = color; }

public:

    /**
     * @brief Constructor that initializes a cell
     * @param isRimCell If the cell is a rim cell
     * @param action What the cell should do next
     * @test A cell should have the right values based on the arguments when created
     */
    Cell(bool isRimCell = false, ACTION action = DO_NOTHING);

    /**
     * @brief Checks if the cell is alive
     * @return If the cell is alive
     * @test Should return true if the cell is alive and false if it is dead
     */
    bool isAlive();

    /**
     * @brief Sets the cells next action to take in its coming update
     * @param action The cells next action
     * @test The nextGenerationAction should be changed to the one given in the argument
     */
    void setNextGenerationAction(ACTION action);

    /**
     * @brief Updates the cell to its new state, based on stored update values.
     * @test A cell with nextGenerationAction value KILL_CELL should be killed
     * @test A cell with nextGenerationAction value IGNORE_CELL or GIVE_CELL_LIFE should have its age incremented by 1
     * @test A cell with a different color in the nextUpdate should be changed to the right color
     * @test A cell with a different value in the nextUpdate should be changed to the right value
     * @test The next action should be reset
     */
    void updateState();

    /**
     * @brief Gets the cell's age
     * @return The age of the cell
     * @test The right age should be returned
     */
    int getAge() { return details.age; }

    /**
     * @brief Gets the color of the cell
     * @return The color of the cell
     * @test The right color should be returned
     */
    COLOR getColor() { return details.color; }

    /**
     * @brief Determines whether the cell is a rim cell, and thus should be immutable
     * @return If the cell is a rim cell
     * @test Should return true if the cell is a rim cell and false if it is not
     */
    bool isRimCell() { return details.rimCell; }

    /**
     * @brief Sets the color the cell will have after its next update.
     * @param nextColor The color the cell will have after its next update
     * @test The next color should be updated to the color given in the argument
     */
    void setNextColor(COLOR nextColor) { this->nextUpdate.nextColor = nextColor; }

    /**
     * @brief Gets the character value of the cell
     * @return The cell's character value
     * @test Should return the right value
     */
    char getCellValue() { return details.value; }

    /**
     * @brief Sets the next character value of the cell, which will be printed to screen.
     * @param value The cell's next character value
     * @test The next value should be updated to the value given in the argument
     */
    void setNextCellValue(char value) { nextUpdate.nextValue = value; }

    /**
     * @brief Sets whether the cell is alive or dead the next generation
     * @param isAliveNext If the cell will be alive the next generation
     * @test If the value true is given as argument, the function isAliveNext should return true when called, and false if the argument was false
     */
    void setIsAliveNext(bool isAliveNext) { nextUpdate.willBeAlive = isAliveNext; }

    /**
     * @brief Will the cell be alive next generation?
     * @return If the cell will be alive the next generation
     * @test Should return true if the cell is alive the next generation and false if it will be dead
     */
    bool isAliveNext() { return nextUpdate.willBeAlive; }

    /**
     * @brief Gets the cells next action.
     * @return The cell's next action
     * @test The right ACTION for the next generation should be returned
     */
    ACTION& getNextGenerationAction() { return nextUpdate.nextGenerationAction; }

};

#endif
