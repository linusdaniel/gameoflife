/**
 * @file        GameOfLife.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef GameOfLifeH
#define GameOfLifeH

#include "Cell_Culture/Population.h"
#include "ScreenPrinter.h"
#include <string>

/**
 * @brief The heart of the simulation, interconnects the main execution with the graphical presentation.
 *
 * @details Creates and manages Population and the ScreenPrinter instances. Will instruct the Population of cells to keep
 * updating as long as the specified number of generations is not reached, and for each iteration instruct
 * ScreenPrinter to write the information on screen.
 *
 * @test Simulation should run without any problems
*/
class GameOfLife {

private:
    Population population;
    ScreenPrinter& screenPrinter;
    int nrOfGenerations;

public:
    /**
     * @brief Constructor for class GameOfLife
     *
     * @param nrOfGenerations The number of generations to iterate over
     * @param evenRuleName The name of the rule to use for even generations
     * @param oddRuleName The name of the rule to use for odd generations
     *
    */
    GameOfLife(int nrOfGenerations, string evenRuleName, string oddRuleName);

    /**
     * @brief Runs the simulation
     *
     * @details Run the simulation for as many generations as been set by the user (default = 500).
     * For each iteration; calculate population changes and print the information on screen.
     *
     * @test Simulation should run until finished or exception is thrown
    */
    void runSimulation();
};

#endif
