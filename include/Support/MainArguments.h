/**
 * @file        MainArguments.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef GAMEOFLIFE_MAINARGUMENTS_H
#define GAMEOFLIFE_MAINARGUMENTS_H

#include "Globals.h"
#include "ScreenPrinter.h"
#include <sstream>
#include <string>

using namespace std;

/**
 * @brief Data structure with values for the initial state of the simulation
 */
struct ApplicationValues {
    bool runSimulation = true;
    string evenRuleName, oddRuleName;
    int maxGenerations = 100;

};

/**
 * @class BaseArgument
 * @brief An abstract base class for arguments for the class MainArgumentsParser
 * @details The derived classes are used for the different arguments provided by the user
 */
class BaseArgument {
protected:
    const string argValue;

    /**
     * @brief Inform the user that no value was provided for the argument
     * @test The function should print a message to the screen
     */
    void printNoValue();

public:
    /**
     * @brief Constructor for the base class
     * @param argValue The name of the argument
     */
    BaseArgument(string argValue) : argValue(argValue) {}

    /**
     * @brief Virtual destructor for BaseArgument
     */
    virtual ~BaseArgument() {}

    /**
     * @brief Pure virtual function for executing functions in the derived classes
     * @param appValues A reference to the ApplicationValues object
     * @param value A pointer to the value of the argument
     */
    virtual void execute(ApplicationValues& appValues, char* value = nullptr) = 0;

    /**
     * @brief Gets a reference to the value of the argument
     * @return A reference to the value of the argument
     * @test The right value should be returned
     */
    const string& getValue() { return argValue; }
};

/**
 * @class HelpArgument
 * @brief Class for the Help argument
 * @details Derived class for the argument for the Help screen
 */
class HelpArgument : public BaseArgument {
public:
    /**
     * @brief Default constructor which sets the argument value to "-h"
     */
    HelpArgument() : BaseArgument("-h") {}

    /**
     * @brief Virtual destructor
     */
    ~HelpArgument() {}

    /**
     * @brief Prints the help screen and exits the simulation
     * @param appValues A reference to the ApplicationValues object
     * @param value A pointer to the value of the argument
     * @test The function should print the help screen
     */
    void execute(ApplicationValues& appValues, char* value);
};

/**
 * @class GenerationsArgument
 * @brief Derived class for the number of generations to simulate over
 * @throws May throw an exception if a non-digit is provided as the generations argument
 * @details Amount of generations to simulate
 */
class GenerationsArgument : public BaseArgument {
public:
    /**
     * @brief Default constructor which sets the argument value to "-g"
     */
    GenerationsArgument() : BaseArgument("-g") {}

    /**
     * @brief Virtual destructor
     */
    ~GenerationsArgument() {}

    /**
     * @brief Sets the value of maxGenerations to the value provided by the user
     * @param appValues A reference to the ApplicationValues object
     * @param generations A pointer to the value of the argument
     * @test The function should change the value maxGenerations to the one provided in the argument
     */
    void execute(ApplicationValues& appValues, char* generations);
};

/**
 * @class WorldsizeArgument
 * @brief Derived class for the population size in the simulation
 * @details Custom population size if the argument is provided by the user
 */
class WorldsizeArgument : public BaseArgument {
public:
    /**
     * @brief Default constructor which sets the argument value to "-s"
     */
    WorldsizeArgument() : BaseArgument("-s") {}

    /**
     * @brief Virtual destructor
     */
    ~WorldsizeArgument() {}

    /**
     * @brief Sets the width and height for the simulation
     * @param appValues A reference to the ApplicationValues object
     * @param dimensions A pointer to the value of the argument
     * @test The function should set the right dimensions
     */
    void execute(ApplicationValues& appValues, char* dimensions);
};

/**
 * @class FileArgument
 * @brief Derived class for the filename
 * @details Initiate population from file if the argument is provided by the user
 */
class FileArgument : public BaseArgument {
public:
    /**
     * @brief Default constructor which sets the argument value to "-f"
     */
    FileArgument() : BaseArgument("-f") {}

    /**
     * Virtual destructor
     */
    ~FileArgument() {}

    /**
     * @brief Sets the filename for the simulation if the value "-f" is provided by the user
     * @param appValues A reference to the ApplicationValues object
     * @param fileNameArg A pointer to the value of the argument
     * @test The function should change the filename
     */
    void execute(ApplicationValues& appValues, char* fileNameArg);
};

/**
 * @class EvenRuleArgument
 * @brief Derived class for the rule for even generations
 * @details Decides which rule is used for even generations
 */
class EvenRuleArgument : public BaseArgument {
public:
    /**
     * @brief Default constructor which sets the argument value to "-er"
     */
    EvenRuleArgument() : BaseArgument("-er") {}

    /**
     * @brief Virtual destructor
     */
    ~EvenRuleArgument() {}

    /**
     * @brief Sets the rule for even generations
     * @param appValues A reference to the ApplicationValues object
     * @param evenRule A pointer to the value of the argument
     * @test The function should change the rule for even generations
     * @bug The function would start the application with the wrong ruleName but it is solved
     */
    void execute(ApplicationValues& appValues, char* evenRule);
};

/**
 * @class OddRuleArgument
 * @brief Derived class for the rule for odd generations
 * @details Decides which rule is used for odd generations
 */
class OddRuleArgument : public BaseArgument {
public:
    /**
     * @brief Default constructor which sets the argument value to "-or"
     */
    OddRuleArgument() : BaseArgument("-or") {}

    /**
     * @brief Virtual destructor
     */
    ~OddRuleArgument() {}

    /**
     * @brief Sets the rule for odd generations
     * @param appValues A reference to the ApplicationValues object
     * @param oddRule A pointer to the value of the argument
     * @test The function should change the rule for odd generations
     * @bug The function would start the application with the wrong ruleName but it is solved
     */
    void execute(ApplicationValues& appValues, char* oddRule);
};

#endif //GAMEOFLIFE_MAINARGUMENTS_H
