/**
 * @file        SupportStructures.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
 *
 * @brief       Various supportive structures to be used throughout the application!
*/

#ifndef GAMEOFLIFE_SUPPORTSTRUCTURES_H
#define GAMEOFLIFE_SUPPORTSTRUCTURES_H

/**
* @brief Constitues a single Point in the simulated world.
*
* @details The Point structure handles x and y (column/row) coordinates in the world of Game of life, and is used
* to map Cell objects to their positions.
*/
struct Point {
    int x, y;

    /**
     * @brief Overloading operator < for comparing Point objects
     *
     * @param other The Point object to be compared to
     * @return if the x value is smaller than that compared to, and if they are the same the y values are compared instead
     * @test The x value should return true if smaller than that compared to and false if it is not
     * @test If the x values are the same the y values should be compared instead
     */
    bool operator < (const Point& other) const {
        if (x == other.x)
            return y < other.y;
        return x < other.x;
    }

};

/**
@brief Data structure holding information about world dimensions in pixels.
*/
struct Dimensions {
    int WIDTH, HEIGHT;
};


#endif //GAMEOFLIFE_SUPPORTSTRUCTURES_H
