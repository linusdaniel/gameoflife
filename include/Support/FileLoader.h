/**
 * @file        FileLoader.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef FileLoaderH
#define FileLoaderH

#include <map>
#include "Cell_Culture/Cell.h"
#include "Globals.h"

using namespace std;

/**
 * @brief Determines starting values for simulation, based on contents of specified file.
 *
 * @details Reads startup values from specified file, containing values for WORLD_DIMENSIONS and cell Population.
 *          Will create the corresponding cells.
 *
 * @test Should properly handle loading population from file
*/
class FileLoader {

public:
    /**
     * @brief Default constructor of FileLoader
     */
    FileLoader() {}

    /**
     * @brief Loads a starting population from a file
     *
     * @param cells Cells read from the file that's pointed to by the global variable fileName
     *
     * @test The wrong filename should throw an exception
     * @test The right world dimensions should be loaded
     */
    void loadPopulationFromFile(map<Point, Cell>& cells);

};

#endif
