/**
 * @file        Globals.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
 * @brief       Globals for the simulation
*/

#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include "SupportStructures.h"

using namespace std;

/**
 * @brief The actual width and height of the used world
 * @test Test by loading file
 */
extern Dimensions WORLD_DIMENSIONS;

/**
 * @brief Name of file to read
 * @test Test by loading file
 */
extern string fileName;


#endif