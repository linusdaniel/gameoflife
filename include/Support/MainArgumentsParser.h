/**
 * @file        MainArgumentsParser.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef mainArgumentsParserH
#define mainArgumentsParserH

#include "MainArguments.h"
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

/**
 * @class MainArgumentsParser
 * @brief A class that identifies arguments provided by the user
 * @details Uses static functions that parses the starting arguments for the application.
 * @bug If given argument -er or -or followed by a word the application will not run printNoValue() and stop as it should, it runs with the standard rules
 * @bug If given argument -g followed by a non digit an exception will be thrown.
 * @bug If an invalid argument is given the application runs anyway, for example $./GameOfLife badValue . The application should instead print the help screen with possible arguments
 */
class MainArgumentsParser {
public:

    /**
     * @brief Arguments provided by the user
     * @param argv An array of possible arguments for the parser
     * @param length The number of arguments
     * @return Returns a reference to an ApplicationValues object for the simulation
     * @test The right application values should be returned
     */
    ApplicationValues& runParser(char* argv[], int length);

private:
    ApplicationValues appValues;

    /**
     * @brief Checks if a given option exists
     * @param begin A pointer to a pointer to the first argument
     * @param end A pointer to a pointer to the last argument
     * @param option A reference to a string with the name of the option
     * @return Returns true if the option exists
     * @test Should return true if the option exists
     */
    bool optionExists(char** begin, char** end, const std::string& option);

    /**
     * @brief Gets the given option value
     * @param begin A pointer to a pointer to the first argument
     * @param end A pointer to a pointer to the last argument
     * @param option A reference to a string with the name of the option
     * @return A pointer to the option
     * @test Should return the right option
     */
    char* getOption(char** begin, char** end, const std::string& option);
};

#endif
