/**
 * @file        RuleFactory.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef RULEFACTORY_H
#define RULEFACTORY_H

#include "GoL_Rules/RuleOfExistence.h"

/**
 * @class RuleFactory
 * @brief Singleton class to handle creation of RulesOfExistence objects.
 *
 * @test The class should create the correct rules and only return reference to the same object
*/
class RuleFactory
{
private:

    /**
     * @brief Default constructor for class RuleFactory
     *
     * @details The constructor is private to ensure no more than one object can be created.
     *
    */
    RuleFactory() {}

public:

    /**
     * @brief Returns an instance of this class
     *
     * @details This function is used to get reference to the one and only RuleFactory object.
     * The instance returned is always the same one because of the static keyword.
     *
     * @return A reference to a static RuleFactory object
     *
     * @test Should always return reference to the same instance of RuleFactory
    */
    static RuleFactory& getInstance();

    /**
     * @brief Creates and returns a rule
     *
     * @details Depending on the rulename cells will be assigned a different rule
     *
     * @param cells A map containing points and cell
     * @param ruleName The name of the rule to be created
     *
     * @return Pointer to a RuleOfExistence
     *
     * @test The correct rule for given ruleName should be created and returned
     * @test If a string of invalid or no value is passed conway rule should be created
    */
    RuleOfExistence* createAndReturnRule(map<Point, Cell>& cells, string ruleName = "conway");
};

#endif