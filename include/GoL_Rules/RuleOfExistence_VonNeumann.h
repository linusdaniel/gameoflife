/**
 * @file        RuleOfExistence_VonNeumann.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H
#define GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H

#include "RuleOfExistence.h"

/**
 * @class RuleOfExistence_VonNeumann
 * @brief Von Neumann's RuleOfExistence, differs from Conway in that only 4 neighbours are accounted for.
 *
 * @details Concrete Rule of existence, implementing Von Neumann's rule.
 * Only difference from Conway is that neighbours are determined using only cardinal directions (N, E, S, W).
 *
 * @test The class should correctly apply it's rule
*/
class RuleOfExistence_VonNeumann : public RuleOfExistence
{
private:

public:

    /**
     * @brief Constructor for the class RuleOfExistence_VonNeumann
     *
     * @details Creates RuleOfExistence_VonNeumann object with values given by the rule.
     *
     * @param cells A map containing points and cells
     *
    */
    RuleOfExistence_VonNeumann(map<Point, Cell>& cells)
            : RuleOfExistence({ 2,3,3 }, cells, CARDINAL, "von_neumann") {}

    /**
     * @brief Virtual deconstructor for the class RuleOfExistence_VonNeumann
     *
    */
    ~RuleOfExistence_VonNeumann() {}

    /**
     * @brief Execute the rule specific for VonNeumann
     *
     * @details The cell will either be killed, keep living or be resurrected.
     *
	 * @test The state of the cellgrid should be updated correctly according to the ruleset
    */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H
