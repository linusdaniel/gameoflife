/**
 * @file        RuleOfExistence_Erik.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H
#define GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H

#include "RuleOfExistence.h"
/**
 * @class RuleOfExistence_Erik
 * @brief Erik's RuleOfExistence, based on Conway's rule while also differentiate the appearance of cells based on their age.
 *
 * @details Concrete Rule of existence, implementing Erik's rule.
 * Sentient lifeforms is rarely created, but once in a while a cell has lived enough generations to become as wise as Erik.
 *
 * - Once a cell has survived a minimum amount of generations it will receive a color to distinguish itself from younger ones.
 * - If such a cell would then survive another set amount of generations, it will be marked with a value of **E**.
 * - In the extreme case, where the cell has achieved above requirements and is determined to be the oldest living cell, it will
 * become a **prime elder**, and have its color changed once again. A generation may only have one such elder.
 *
 * @test The class should correctly apply it's rule
*/
class RuleOfExistence_Erik : public RuleOfExistence
{
private:
    char usedCellValue;	// char value to differentiate very old cells.
    Cell* primeElder;

    /**
     * @brief Decides color and char value for the cell
     *
     * @details With age comes experience. Cells older than 5 generations receives an old age color.
     * If the cell is older than 10 generations, it also gets the value 'E' (for Erik) signifying
     * a sentient life form of great wisdom. Thus proving, that intelligent life can be created using
     * cellular automata.
     *
     * @param cell The cell to erikfy
     * @param action Action to be taken on the cell next generation
     *
	 * @test Color and/or char value should change if cell age exceeds the given limit
    */
    void erikfyCell(Cell& cell, ACTION action);

    /**
     * @brief Sets the prime elder
     *
     * @param newElder Pointer to the cell that should become the new elder
     *
	 * @test The state of the cellgrid should be updated correctly according to the ruleset
    */
    void setPrimeElder(Cell* newElder);

public:

    /**
     * @brief Constructor for the class RuleOfExistence_Erik
     *
     * @details Creates RuleOfExistence_Erik object with values given by the rule.
     *
     * @param cells A map containing points and cells
     *
    */
    RuleOfExistence_Erik(map<Point, Cell>& cells)
            : RuleOfExistence({2,3,3}, cells, ALL_DIRECTIONS, "erik"), usedCellValue('E') {
        primeElder = nullptr;
    }

    /**
     * @brief Virtual deconstructor for the class RuleOfExistence_Erik
     *
    */
    ~RuleOfExistence_Erik() {}

    /**
     * @brief Execute the rule specific for Erik
     *
     * @details The cell will either be killed, keep living or be resurrected. The cell is also sent to be "erikfied".
     *
	 * @test The state of the cellgrid should be updated correctly according to the ruleset
    */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H
