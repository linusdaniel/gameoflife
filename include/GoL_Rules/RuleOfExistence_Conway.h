/**
 * @file        RuleOfExistence_Conway.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H
#define GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H

#include "RuleOfExistence.h"

/**
 * @class RuleOfExistence_Conway
 * @brief Conway's RuleOfExistence, applying actions based on PopulationLimits on all 8 surrounding neighbours.
 *
 * @details Concrete RuleOfExistence, implementing Conway's rule of determining alive neighbours surrounding the cell
 * by checking all 8 directions, 4 x Cardinal + 4 x Diagonal. PopulationLimits are set as;
 *
 * UNDERPOPULATION	< 2*	**Cell dies of loneliness**
 * OVERPOPULATION	> 3*	**Cell dies of overcrowding**
 * RESURRECTION		= 3*	**Cell is infused with life**
 *
 * @test The class should correctly apply it's rule
*/
class RuleOfExistence_Conway : public RuleOfExistence
{
private:

public:
    /**
     * @brief Constructor for the class RuleOfExistence_Conway
     *
     * @details Creates RuleOfExistence_Conway object with values given by the rule.
     *
     * @param cells A map containing points and cells
     *
    */
    RuleOfExistence_Conway(map<Point, Cell>& cells)
            : RuleOfExistence({ 2,3,3 }, cells, ALL_DIRECTIONS, "conway") {}

    /**
     * @brief Virtual deconstructor for the class RuleOfExistence_Conway
     *
    */
    ~RuleOfExistence_Conway() {}

    /**
     * @brief Execute the rule specific for Conway
     *
     * @details The cell will either be killed, keep living or be resurrected
     *
     * @test The state of the cellgrid should be updated correctly according to the ruleset
    */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H
