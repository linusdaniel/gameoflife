/**
 * @file        RuleOfExistence.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/
#ifndef RULEOFEXISTENCE_H
#define RULEOFEXISTENCE_H

#include <string>
#include <map>
#include <vector>
#include "Cell_Culture/Cell.h"
#include "Support/Globals.h"
using namespace std;

/*
These rules lies at the heart
of the simulation, and determines the fate of each cell in the world population.
*/

/**
 * @brief Data structure for storing population limits. Used by rules to determine what ACTION to make.
 *
*/
struct PopulationLimits {
    int UNDERPOPULATION, // cell dies of loneliness
            OVERPOPULATION, // cell dies of over crowding
            RESURRECTION; // cell lives on / is resurrected
};

/**
 * @brief Data structure for storing directional values. Used by rules to determine neighbouring cells.
 *
*/
struct Directions {
    int HORIZONTAL, VERTICAL;
};

// All directions; N, E, S, W, NE, SE, SW, NW
const vector<Directions> ALL_DIRECTIONS{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 },{ 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

// Cardinal directions; N, E, S, W
const vector<Directions> CARDINAL{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 } };

// Diagonal directions; NE, SE, SW, NW
const vector<Directions> DIAGONAL{ { 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

/**
 * @class RuleOfExistence
 * @brief Abstract base class, upon which concrete rules will derive.
 *
 * @details The derivations of RuleOfExistence is what determines the culture of Cell Population. Each rule implements
 * specific behaviours and so may execute some parts in different orders. In order to accommodate this
 * requirement RuleOfExistence will utilize a **Template Method** design pattern, where all derived rules
 * implements their logic based on the virtual method executeRule().
 *
 * @test The class should correctly apply it's rule
*/
class RuleOfExistence {
protected:
    string ruleName;

    // Reference to the population of cells
    map<Point, Cell>& cells;

    // Amounts of alive neighbouring cells, with specified limits
    const PopulationLimits POPULATION_LIMITS;

    // The directions, by which neighbouring cells are identified
    const vector<Directions>& DIRECTIONS;

    /**
     * @brief Counts how many neighbours are alive
     *
     * @details The directions specified by the rule determines how the alive neighbours are calculated.
     *
     * @param currentPoint The point of the cell to calculate alive neighbours for
     *
     * @return The number of alive neighbours
     *
     * @test Should return the right amount of living neighbours
    */
    int countAliveNeighbours(Point currentPoint);

    /**
     * @brief Determines which action to make, based on the amount of alive neighbours
     *
     * @details
     *
     * @param aliveNeighbours The number of alive alive neighbours
     * @param isAlive Whether the cell is alive or not
     *
     * @return The action to make
     *
     * @test The action returned should be correct given the alive neighbours and if cell is alive or not
    */
    ACTION getAction(int aliveNeighbours, bool isAlive);

public:

    /**
     * @brief Constructor for the class RuleOfExistence
     *
     * @param limits The numbers used to determine which action to take
     * @param cells A map containing points and cells
     * @param DIRECTIONS The directions to be used when calculating alive neighbours
     * @param ruleName The name of the rule to be used
     *
    */
    RuleOfExistence(PopulationLimits limits, map<Point, Cell>& cells, const vector<Directions>& DIRECTIONS, string ruleName)
            : POPULATION_LIMITS(limits), cells(cells), DIRECTIONS(DIRECTIONS), ruleName(ruleName) {}

    /**
     * @brief Virtual deconstructor for the class RuleOfExistence
     *
     * @details
     *
    */
    virtual ~RuleOfExistence() {}

    /**
     * @brief Execute rule, in order specific to the concrete rule, by utilizing template method DP
     *
     * @details Pure virtual function to be implemented in derived classes
     *
    */
    virtual void executeRule() = 0;

    /**
     * @brief Returns the name of the rule
     *
     * @return The name of the rule that is being used
     *
     * @test The correct rulename should be returned
    */
    string getRuleName() { return ruleName; }
};

#endif