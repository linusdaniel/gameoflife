/**
 * @file        ScreenPrinter.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/



#ifndef screenPrinterH
#define screenPrinterH


#include "../terminal/terminal.h"
#include "Cell_Culture/Population.h"

/**
 * @brief Responsible for visually representing the simulation world on screen.
 *
 * @details The main purpose of this class is to handle all of the "communication"
 * with the user and to present the simulation world to the user.
 *
 * The class uses the singleton pattern to restrict the number of objects that can be created of this class to one.
 *
 * @test The class should correctly print the wanted information
 * @test This class should be tested "manually". Everything it has to do with is printing information to the user.
 * So it's best suited that the "result" of these functions are viewed and verified manually.
*/
class ScreenPrinter {

private:
    Terminal terminal;

    /**
     * @brief Default constructor for class ScreenPrinter
     *
     * @details The constructor is private to ensure no more than one object can be created.
     */
    ScreenPrinter() {}

public:

    /**
     * @brief Returns an instance of this class
     *
     * @details This function is used to get reference to the one and only ScreenPrinter object.
     * The instance returned is always the same one because of the static keyword.
     *
     * @return A reference to a static ScreenPrinter object
     *
     * @test Should always return reference to the same instance of ScreenPrinter
     */
    static ScreenPrinter& getInstance() {
        static ScreenPrinter instance;
        return instance;
    }

    /**
     * @brief Prints the population grid to the screen
     *
     * @details
     *
     * @param population The population to be printed to the screen
     *
     * @test The cellgrid should be correctly printed to the screen
     */
    void printBoard(Population& population);

    /**
     * @brief Prints the help screen to the screen
     *
     * @test The help screen should be correctly printed to the screen
     */
    void printHelpScreen();

    /**
     * @brief Prints the supplied message to the screen
     *
     * @param message Message to be printed to screen
     *
     * @test The message should be correctly printed to the screen
     */
    void printMessage(string message);

    /**
     * @brief Clears the screen
     *
     * @test The screen should be cleared
     */
    void clearScreen();
};

#endif
