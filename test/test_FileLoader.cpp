/**
 * @file        test_FileLoader.cpp
 * @author      Daniel Berg
 * @date        November 2017
 * @version     0.1
*/

#include "catch.hpp"
#include "Support/FileLoader.h"

SCENARIO("Loading population from file with valid name") {
    GIVEN("File with valid name") {
        fileName = "../test/FileLoaderTestPopulation.txt";
        /*
        File values:

        5x5
        10101
        10101
        10101
        10101
        10101
        */

        WHEN("File is loaded") {

            map<Point, Cell> fileCells;
            FileLoader loader;
            loader.loadPopulationFromFile(fileCells);

            THEN("Width should be 5") {
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 5);
            }

            THEN("Height should be 5") {
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 5);
            }

            THEN("Population should match file population") {
                for (int row = 0; row <= WORLD_DIMENSIONS.HEIGHT + 1; row++) {

                    for (int column = 0; column <= WORLD_DIMENSIONS.WIDTH + 1; column++) {

                        // Check for rimcells
                        if (column == 0 || row == 0
                            || column == WORLD_DIMENSIONS.WIDTH + 1
                            || row == WORLD_DIMENSIONS.HEIGHT + 1) {

                            //controlCells[Point{column, row}] = Cell(true); // create a cell with rimCell state set to true
                            REQUIRE((fileCells[{column, row}].isRimCell()));
                        } else {

                            if (column % 2 == 0) {
                                // Kolumerna 0, 2, 4 osv... ska innehålla döda celler
                                REQUIRE_FALSE((fileCells[{column, row}].isAlive()));
                            } else {
                                // Kolumerna 1, 3, 5 osv... ska innehålla levande celler
                                REQUIRE((fileCells[{column, row}].isAlive()));
                            }
                        }
                    }
                }
            }
        }

        // Resets WORLD_DIMENSIONS for other tests
        WORLD_DIMENSIONS.WIDTH = 80;
        WORLD_DIMENSIONS.HEIGHT = 24;
    }
}

SCENARIO("Loading population from file with invalid name or nonexistent file") {
    GIVEN("File with invalid name/nonexistent") {
        // Makes sure the file given by fileName will be empty
        fileName = "";
        map<Point, Cell> cells;

        WHEN("File is loaded") {
            FileLoader loader;

            // Information for the tester
            cout << "THIS IS A PART OF THE TESTS!" << endl;

            THEN("It should throw an exception") {
                REQUIRE_THROWS_AS(loader.loadPopulationFromFile(cells), ios_base::failure);
            }

            // Information for the tester
            cout << "TESTS ARE STILL RUNNING!" << endl;
        }
    }
}