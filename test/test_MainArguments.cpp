/**
 * @file        test_MainArguments.cpp
 * @author      Linus Ängeskog
 * @date        October 2017
 * @version     0.1
*/

#include "catch.hpp"
#include "Support/MainArgumentsParser.h"

SCENARIO("Arguments with different values"){
    GIVEN("A help argument and default application values"){
        ApplicationValues appValues;
        HelpArgument help;

        THEN("The appValue should be -h") {
            REQUIRE(help.getValue() == "-h");
        }
        AND_THEN("appValues should be set to run the simulation"){
            REQUIRE(appValues.runSimulation == true);
        }

        AND_WHEN("the help argument is executed") {
            char *noValue = nullptr;
            help.execute(*&appValues, noValue);

            THEN("The help screen should be printed and runSimulation should be false") {
                REQUIRE(appValues.runSimulation == false);
            }
        }
    }

    GIVEN("A generations argument and default application values") {
        ApplicationValues appValues;
        GenerationsArgument genArg;

        THEN("The appvalue should be -g") {
            REQUIRE(genArg.getValue() == "-g");
        }

        AND_WHEN("the generations argument is executed with a value") {
            char* gen;
            gen = "55";
            genArg.execute(*&appValues, gen);

            THEN("The generations value should be changed to the right value") {
                REQUIRE(appValues.maxGenerations == 55);
            }
            AND_THEN("runSimulation should be true and printNoValue should print info") {
                REQUIRE(appValues.runSimulation == true);
            }
        }

        AND_WHEN("Another generations argument is executed without a value") {
            GenerationsArgument genArg2;
            char* noValue = nullptr;
            genArg2.execute(*&appValues, noValue);

            THEN("maxGenerations should not be the default 100") {
                REQUIRE(appValues.maxGenerations == 100);
            }
            AND_THEN("runSimulation should be false and printNoValue should print info") {
                REQUIRE(appValues.runSimulation == false);
            }
        }
    }

    GIVEN("A worldsize argument and default application values") {
        ApplicationValues appValues;
        WorldsizeArgument worldsize;

        THEN("The appvalue should be -s") {
            REQUIRE(worldsize.getValue() == "-s");
        }

        AND_WHEN("the worldsize argument is executed with a value") {
            char* size;
            size = "33x78";
            worldsize.execute(*&appValues, size);

            THEN("The worldsize dimensions should be changed") {
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 33);
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 78);
            }
            AND_THEN("runSimulation should be true") {
                REQUIRE(appValues.runSimulation == true);
            }
        }
        AND_WHEN("the worldsize argument is executed without a value") {
            char* noValue = nullptr;
            worldsize.execute(*&appValues, noValue);

            THEN("The world dimensions should have stayed the same") {
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 33);
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 78);
            }
            AND_THEN("runSimulation should be false and printNoValue should print info") {
                REQUIRE(appValues.runSimulation == false);
            }
            // Reset values
            WORLD_DIMENSIONS.WIDTH = 80;
            WORLD_DIMENSIONS.HEIGHT = 24;
        }

    }

    GIVEN("A file argument and default application values") {
        ApplicationValues appValues;
        FileArgument fileArg;

        THEN("The appvalue should be -f") {
            REQUIRE(fileArg.getValue() == "-f");
        }

        AND_WHEN("the file argument is executed without a value") {
            char* noValue = nullptr;
            fileArg.execute(*&appValues, noValue);

            THEN("The global filename should not be changed") {
                REQUIRE(fileName == "");
            }
            AND_THEN("runSimulation should be false and printNoValue should print info") {
                REQUIRE(appValues.runSimulation == false);
            }
        }
        AND_WHEN("the file argument is executed with a value") {

            char* name;
            name = "newname";
            fileArg.execute(*&appValues, name);

            THEN("The filename should have changed") {
                REQUIRE(fileName == name);
            }
            AND_THEN("runSimulation should be true") {
                REQUIRE(appValues.runSimulation == true);
            }
        }
    }

    GIVEN("An Even rule argument and default application values") {
        ApplicationValues appValues;
        EvenRuleArgument evenRule;

        THEN("The appvalue should be -er") {
            REQUIRE(evenRule.getValue() == "-er");
        }

        AND_WHEN("the even rule argument is executed with a value") {
            char* rule;
            rule = "von_neumann";
            evenRule.execute(*&appValues, rule);

            THEN("The even rule should be changed but the odd rule should still be the default value") {
                REQUIRE(appValues.evenRuleName == "von_neumann");
                REQUIRE(appValues.oddRuleName == "");
            }
            AND_THEN("runSimulation should be true") {
                REQUIRE(appValues.runSimulation == true);
            }
        }
        AND_WHEN("the even rule argument is executed without a value") {
            char* noValue = nullptr;
            evenRule.execute(*&appValues, noValue);

            THEN("The even rule name should have stayed the same") {
                REQUIRE(appValues.evenRuleName == "");
                REQUIRE(appValues.oddRuleName == "");
            }
            AND_THEN("runSimulation should be false and printNoValue should print info") {
                REQUIRE(appValues.runSimulation == false);
            }
        }
    }

    GIVEN("An Odd rule argument and default application values") {
        ApplicationValues appValues;
        OddRuleArgument oddRule;

        THEN("The appvalue should be -or") {
            REQUIRE(oddRule.getValue() == "-or");
        }

        AND_WHEN("the odd rule argument is executed with a value") {
            char* rule;
            rule = "erik";
            oddRule.execute(*&appValues, rule);

            THEN("The odd rule should be changed but the even rule should still be the default value") {
                REQUIRE(appValues.evenRuleName == "");
                REQUIRE(appValues.oddRuleName == "erik");
            }
            AND_THEN("runSimulation should be true") {
                REQUIRE(appValues.runSimulation == true);
            }
        }
        AND_WHEN("the odd rule argument is executed without a value") {
            char* noValue = nullptr;
            oddRule.execute(*&appValues, noValue);

            THEN("The odd rule name should have stayed the same") {
                REQUIRE(appValues.evenRuleName == "");
                REQUIRE(appValues.oddRuleName == "");
            }
            AND_THEN("runSimulation should be false and printNoValue should print info") {
                REQUIRE(appValues.runSimulation == false);
            }
        }
    }

}
