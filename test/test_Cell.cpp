/**
 * @file        test_Cell.cpp
 * @author      Daniel Berg
 * @date        November 2017
 * @version     0.1
*/

#include "catch.hpp"
#include "GameOfLife.h"

SCENARIO("Killing a cell") {
    GIVEN("A non rim cell") {
        Cell cell(false, GIVE_CELL_LIFE);
        cell.setNextGenerationAction(KILL_CELL);
        cell.setIsAliveNext(false);

        THEN("Cell should not be a rimCell") {
            REQUIRE_FALSE(cell.isRimCell());
        }

        THEN("Cell should be alive") {
            REQUIRE(cell.isAlive());
        }

        THEN("Age of cell should be 1") {
            REQUIRE(cell.getAge() == 1);
        }

        THEN("Cell color should be LIVING") {
            REQUIRE(cell.getColor() == STATE_COLORS.LIVING);
        }

        THEN("Next generation action should be KILL_CELL") {
            REQUIRE(cell.getNextGenerationAction() == KILL_CELL);
        }

        THEN("Cell should not be alive next") {
            REQUIRE_FALSE(cell.isAliveNext());
        }

        WHEN("Cell state is updated") {
            cell.updateState();

            THEN("Cell should be dead") {
                REQUIRE_FALSE(cell.isAlive());
            }

            THEN("Age of cell should be 0") {
                REQUIRE(cell.getAge() == 0);
            }

            THEN("Next generation action should be DO_NOTHING (reset)") {
                REQUIRE(cell.getNextGenerationAction() == DO_NOTHING);
            }
        }
    }
}

SCENARIO("Resurrecting a cell") {
    GIVEN("A non rim cell") {
        Cell cell(false, KILL_CELL);
        cell.setNextGenerationAction(GIVE_CELL_LIFE);
        cell.setIsAliveNext(true);

        THEN("Cell should not be a rimCell") {
            REQUIRE_FALSE(cell.isRimCell());
        }

        THEN("Cell should be dead") {
            REQUIRE_FALSE(cell.isAlive());
        }

        THEN("Age of cell should be 0") {
            REQUIRE(cell.getAge() == 0);
        }

        THEN("Cell color should be DEAD") {
            REQUIRE(cell.getColor() == STATE_COLORS.DEAD);
        }

        THEN("Next generation action should be KILL_CELL") {
            REQUIRE(cell.getNextGenerationAction() == GIVE_CELL_LIFE);
        }

        THEN("Cell should be alive next") {
            REQUIRE(cell.isAliveNext());
        }

        WHEN("Cell status is updated") {
            cell.updateState();

            THEN("Cell should be alive") {
                REQUIRE(cell.isAlive());
            }

            THEN("Age of cell should be 1") {
                REQUIRE(cell.getAge() == 1);
            }

            THEN("Next generation action should be DO_NOTHING (reset)") {
                REQUIRE(cell.getNextGenerationAction() == DO_NOTHING);
            }
        }
    }
}

SCENARIO("Changing color and value of a cell") {
    GIVEN("A non rim cell") {
        Cell cell(false, DO_NOTHING);
        cell.setNextCellValue('B');
        cell.setNextColor(STATE_COLORS.ELDER);

        WHEN("Cell status is updated") {
            cell.updateState();

            THEN("Color should be ELDER") {
                REQUIRE(cell.getColor() == STATE_COLORS.ELDER);
            }

            THEN("Cellvalue should be 'B'") {
                REQUIRE(cell.getCellValue() == 'B');
            }
        }
    }
}

SCENARIO("Cell being in the rim") {
    GIVEN("A rimcell") {
        Cell cell(true, DO_NOTHING);

        THEN("Cell should be a rimCell") {
            REQUIRE(cell.isRimCell());
        }

        THEN("Cell should be dead because it is a rimcell") {
            REQUIRE_FALSE(cell.isAlive());
        }
    }
}