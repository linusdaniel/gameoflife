/**
 * @file        test_Population.cpp
 * @author      Daniel Berg
 * @date        November 2017
 * @version     0.1
*/

#include "catch.hpp"
#include "Cell_Culture/Population.h"
#include "Support/Globals.h"
#include <thread>
#include <chrono>

SCENARIO("Generating random populations") {
    GIVEN("2 random populations") {

        // Ser till att filnamnet är tomt så att populationerna slumpas
        fileName = "";

        Population pop1, pop2;
        pop1.initiatePopulation("erik", "erik");

        THEN("Height should be 24") {
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
        }

        THEN("Width should be 80") {
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
        }

        // 82*26 to account for rimcells
        THEN("pop2 should contain default amount of cells") {
            REQUIRE(pop1.getTotalCellPopulation() == 82*26);
        }

        THEN("Cell at point 0,0 should be rimcell") {
            REQUIRE(pop1.getCellAtPosition({0,0}).isRimCell());
        }

        // Makes the program stop for a second so a different seed can be used for generation
        this_thread::sleep_for(chrono::seconds(1));
        pop2.initiatePopulation("erik", "erik");

        THEN("Height should be 24") {
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
        }

        THEN("Width should be 80") {
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
        }

        // 82*26 to account for rimcells
        THEN("pop3 should contain default amount of cells") {
            REQUIRE(pop2.getTotalCellPopulation() == 82*26);
        }

        THEN("Cell at point 0,0 should be rimcell") {
            REQUIRE(pop2.getCellAtPosition({0,0}).isRimCell());
        }

        WHEN("Populations are compared") {
            int sameCell = 0;
            for (int row = 0; row <= WORLD_DIMENSIONS.HEIGHT + 1; row++) {

                for (int column = 0; column <= WORLD_DIMENSIONS.WIDTH + 1; column++) {

                    Cell cell1 = pop1.getCellAtPosition({column, row});
                    Cell cell2 = pop2.getCellAtPosition({column, row});

                    if (cell1.isAlive() == cell2.isAlive()
                        && cell1.getColor() == cell2.getColor()
                        && cell1.getAge() == cell2.getAge()
                        && cell1.getCellValue() == cell2.getCellValue()
                        && cell1.isAliveNext() == cell2.isAliveNext()) {
                        sameCell++;
                    }
                }
            }

            THEN("sameCell shouldn't equal totalCells") {
                REQUIRE_FALSE(sameCell == pop1.getTotalCellPopulation());
            }
        }
    }
}

SCENARIO("Population loaded from file") {
    GIVEN("Predetermined population") {

        // Ser till att populationen läses från rätt fil
        fileName = "../Population_Seed.txt";
        /*
        File info:

        20x10
        00000000000000000000
        00000010000011001100
        00111000010010000010
        01110001010011001100
        00010100001000111010
        01000100000110001010
        00110011000110011000
        00001110000110011000
        01000001000011001100
        00000000000000000000
         */

        Population pop3;
        pop3.initiatePopulation("erik", "erik");

        // 22*12 to account for rimcells
        THEN("total amount of cells should be 22*12") {
            REQUIRE(pop3.getTotalCellPopulation() == 22*12);
        }

        THEN("Height should be 10") {
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 10);
        }

        THEN("Width should be 20") {
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 20);
        }

        THEN("Cell at 0,0 should be rimcell") {
            REQUIRE(pop3.getCellAtPosition({0,0}).isRimCell());
        }

        THEN("Cell at 2,9 should be alive") {
            REQUIRE(pop3.getCellAtPosition({2,9}).isAlive());
        }

        THEN("Cell at 2,2 should be dead") {
            REQUIRE_FALSE(pop3.getCellAtPosition({2,2}).isAlive());
        }
    }
}

SCENARIO("Calculating next generation") {
    GIVEN("Predetermined population") {
        Population pop4;
        pop4.initiatePopulation("erik", "erik");
        map<Point, Cell> previousGrid;

        for (int row = 0; row <= WORLD_DIMENSIONS.HEIGHT + 1; row++) {

            for (int column = 0; column <= WORLD_DIMENSIONS.WIDTH + 1; column++) {

                Point point1{column, row};

                previousGrid[point1] = pop4.getCellAtPosition(point1);
            }
        }

        // 22*12 to account for rimcells
        THEN("total amount of cells should be 22*12") {
            REQUIRE(pop4.getTotalCellPopulation() == 22*12);
        }

        THEN("Height should be 10") {
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 10);
        }

        THEN("Width should be 20") {
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 20);
        }

        WHEN("New generation is calculated") {

            // Calculate new generation twice since nothing happens on the first one
            pop4.calculateNewGeneration();
            pop4.calculateNewGeneration();

            int sameCell = 0;

            for (int row = 0; row <= WORLD_DIMENSIONS.HEIGHT + 1; row++) {

                for (int column = 0; column <= WORLD_DIMENSIONS.WIDTH + 1; column++) {

                    Point point2{column, row};

                    Cell cell3 = previousGrid[point2];
                    Cell cell4 = pop4.getCellAtPosition(point2);

                    if (cell3.isAlive() == cell4.isAlive()
                        && cell3.getColor() == cell4.getColor()
                        && cell3.getAge() == cell4.getAge()
                        && cell3.getCellValue() == cell4.getCellValue()
                        && cell3.isAliveNext() == cell4.isAliveNext()) {
                        ++sameCell;
                    }
                }
            }

            THEN("sameCell shouldn't equal total number of cells") {
                REQUIRE_FALSE(sameCell == pop4.getTotalCellPopulation());
            }

            // 22*12 to account for rimcells
            THEN("total amount of cells should still be 22*12") {
                REQUIRE(pop4.getTotalCellPopulation() == 22*12);
            }

            THEN("Height should still be 10") {
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 10);
            }

            THEN("Width should still be 20") {
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 20);
            }

        }

        // Återställer filnamnet till sitt tillstånd innan test
        fileName = "";
    }
}