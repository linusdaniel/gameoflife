/**
 * @file        test_MainArgumentsParser.cpp
 * @author      Linus Ängeskog
 * @date        October 2017
 * @version     0.1
*/

#include "catch.hpp"
#include "Support/MainArgumentsParser.h"

SCENARIO("Parsing with one argument") {

    std::cout << endl << "=========== TESTING PARSER =============" << endl << endl;

    GIVEN("One help argument to the parser") {

        int argc = 2;
        char* argv[argc];
        argv[1] = "-h";

        MainArgumentsParser parser;

        std::cout << endl << "Help screen: " << endl;
        ApplicationValues appValues = parser.runParser(argv, argc);

        THEN("The help screen should be printed and the rule names should both be conway") {

            REQUIRE(appValues.evenRuleName == "conway");
            REQUIRE(appValues.oddRuleName == appValues.evenRuleName);
        }
        AND_THEN("runSimulation should be false") {
            REQUIRE_FALSE(appValues.runSimulation);
        }

    }

    GIVEN("One worldsize argument"){
        int argc = 2;
        char* argv[argc];
        char* s = "-s";
        char* choice = "58x62";

        argv[0] = s;
        argv[1] = choice;


        MainArgumentsParser parser;
        ApplicationValues appValues = parser.runParser(argv, argc);

        THEN("The WORLD_DIMENSIONS should be changed") {
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 58);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 62);

        }
    }

    GIVEN("One generations argument"){
        int argc = 2;
        char* argv[argc];
        char* g = "-g";
        char* choice = "199";

        argv[0] = g;
        argv[1] = choice;

        MainArgumentsParser parser;
        ApplicationValues appValues = parser.runParser(argv, argc);

        THEN("The maxGenerations value should be changed") {
            REQUIRE(appValues.maxGenerations == 199);

        }
    }

    // Set the world dimensions values to the default values
    WORLD_DIMENSIONS.WIDTH = 80;
    WORLD_DIMENSIONS.HEIGHT = 24;
}

SCENARIO("Parsing with two arguments") {
    GIVEN("Two arguments for rule names") {
        int argc = 4;
        char* argv[argc];
        argv[0] = "-er";
        argv[1] = "von_neumann";
        argv[2] = "-or";
        argv[3] = "erik";

        MainArgumentsParser parser;
        ApplicationValues appValues = parser.runParser(argv, argc);

        THEN("Both rules should be changed from the default conway") {
            REQUIRE(appValues.evenRuleName == "von_neumann");
            REQUIRE(appValues.oddRuleName == "erik");
        }
    }
}


SCENARIO("With a false argument") {

    GIVEN("A false argument to the parser") {
        int argc = 2;
        char* argv[argc];
        argv[0] = "-er";
        argv[1] = "falseArgument";

        MainArgumentsParser parser;
        ApplicationValues appValues;

        THEN("runSimulation should be true") {
            REQUIRE(appValues.runSimulation);
        }
        AND_WHEN("The parser has run") {

            appValues = parser.runParser(argv, argc);

            THEN("printNoValue should be executed and runSimulation should be false") {
                REQUIRE_FALSE(appValues.runSimulation);
            }
        }
    }

    GIVEN("A false argument where the argument should be a number but is not") {
        int argc = 2;
        char* argv[argc];
        argv[0] = "-g";
        argv[1] = "falseArgument";

        MainArgumentsParser parser;
        ApplicationValues appValues;

        THEN("An exception should be thrown") {
            REQUIRE_THROWS(appValues = parser.runParser(argv, argc));
        }
    }

    std::cout << endl << "=========== END TESTING PARSER  ============" << endl << endl;
}
