/**
 * @file        test_RuleFactory_RuleOfExistence.cpp
 * @author      Daniel Berg
 * @date        November 2017
 * @version     0.1
*/

#include "catch.hpp"
#include "GameOfLife.h"

SCENARIO("Comparing two instances of RuleFactory") {
    GIVEN("Reference to two \"instance objects\"") {
        RuleFactory& rule1 = RuleFactory::getInstance();
        RuleFactory& rule2 = RuleFactory::getInstance();

        THEN("The instances should reference the same object") {
            REQUIRE(&rule1 == &rule2);
        }
    }
}

SCENARIO("Creation of rules and fetching their name") {
    map<Point, Cell> cells = { {{1,0}, Cell()} };

    GIVEN("Rulename erik") {

        string ruleName = "erik";

        RuleOfExistence* rule3 = RuleFactory::getInstance().createAndReturnRule(cells, ruleName);

        THEN("RuleOfExistence_Erik should be created hence rulename should match the created rules name") {
            REQUIRE(rule3->getRuleName() == ruleName);
        }

        delete rule3;
    }

    GIVEN("Rulename von_neumann") {

        string ruleName = "von_neumann";

        RuleOfExistence* rule4 = RuleFactory::getInstance().createAndReturnRule(cells, ruleName);

        THEN("RuleOfExistence_Von_Neumann should be created hence rulename should match the created rules name") {
            REQUIRE(rule4->getRuleName() == ruleName);
        }

        delete rule4;
    }

    GIVEN("No ruleName") {

        RuleOfExistence* rule5 = RuleFactory::getInstance().createAndReturnRule(cells);

        THEN("RuleOfExistence_Conway should be created hence rulename should be conway") {
            REQUIRE(rule5->getRuleName() == "conway");
        }

        delete rule5;
    }

    GIVEN("An invalid ruleName") {

        string invalidRuleName = "xyz123";
        RuleOfExistence* rule6 = RuleFactory::getInstance().createAndReturnRule(cells);

        THEN("RuleOfExistence_Conway should be created hence rulename should be conway") {
            REQUIRE(rule6->getRuleName() == "conway");
        }

        delete rule6;
    }
}