/**
 * @file        test_SupportStructures.cpp
 * @author      Linus Ängeskog
 * @date        October 2017
 * @version     0.1
*/

#include "catch.hpp"
#include "Support/SupportStructures.h"

SCENARIO("Comparing two Points") {
    GIVEN("Two Points with different x values") {
        Point a{1, 2};
        Point b{2, 3};

        THEN("Point a should be smaller than point b") {
            REQUIRE(a < b);
        }
        AND_THEN("Point b should not be smaller than point a") {
            REQUIRE_FALSE(b < a);
        }
    }
    GIVEN("Two Points with the same x value") {
        Point a{2, 3};
        Point b{2, 1};

        THEN("Point b should be smaller than point a") {
            REQUIRE(b < a);
        }
        AND_THEN("Point a should not be smaller than point b") {
            REQUIRE_FALSE(a < b);
        }

    }
}


