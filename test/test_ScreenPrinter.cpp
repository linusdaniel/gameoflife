/**
 * @file        test_ScreenPrinter.cpp
 * @author      Daniel Berg
 * @date        November 2017
 * @version     0.1
*/

#include "catch.hpp"
#include "ScreenPrinter.h"

SCENARIO("Comparing two instances of ScreenPrinter") {
    GIVEN("Reference to two \"instance objects\"") {
        ScreenPrinter& screen1 = ScreenPrinter::getInstance();
        ScreenPrinter& screen2 = ScreenPrinter::getInstance();

        THEN("The instances should reference the same object") {
            REQUIRE(&screen1 == &screen2);
        }
    }
}