/**
 * @file        main.cpp
 * @Author      Erik Ström
 * @Date        October 2017
 * @Version     0.1
*/

/**
 * @mainpage GameOfLife
 *
 * Detta är en implementation av Game of Life som uppfanns 1970 av matematikern John Conway.
 * Game of Life är ett exempel på cellulär automata som i detta program representeras av ett 2-dimensionellt rutmönster uppbyggt av celler.
 * Spelet är ett "nollspelare spel" vad detta betyder är att evolutionen av cellerna bestäms av det initiala tillståndet och ingen input kan eller behövs göras efter spelet har startat.
 * Själva spelandet går därför ut på att skapa dessa initiala tillstånden och observera hur de utvecklas eller för de avancerade "spelarna" skaoa möster med speciella egenskaper.
 *
 * Klassiska regler:
 * * En​ ​levande​ ​cell​ ​som​ ​har​ ​färre​ ​än​ ​två​ ​levande​ ​grannar​ ​dör​ ​av​ ​under-population. 
 * * En​ ​levande​ ​cell​ ​som​ ​har​ ​två​ ​till​ ​tre​ ​levande​ ​grannar​ ​lever​ ​vidare​ ​till​ ​nästa​ ​generation. 
 * * En​ ​levande​ ​cell​ ​som​ ​har​ ​fler​ ​än​ ​tre​ ​levande​ ​grannar​ ​dör​ ​av​ ​över-population.
 * * En​ ​död​ ​cell​ ​som​ ​har​ ​exakt​ ​tre​ ​levande​ ​grannar​ ​börjar​ ​leva​ ​(reproduktion).
 *
 * @ref about_the_authors
 */

/**
 * @page about_the_authors This page presents the authors of this project
 *
 * @section Author Daniel Berg
 *
 * Mitt namn är Daniel Berg och är en av dem som ansvarar för detta projekt.
 * Lite kort info om mig själv:
 * * Jag är 21 år gammal.
 * * Bor i Falkenberg, Halland.
 * * Distansstudent på miun med inriktning programvaruteknik.
 *
 * Har alltid haft stort datorintresse och inte bara för spel som många andra utan för hur datorer/program fungerar.
 * Dock har jag aldrig riktigt varit förtjust i hårdvarudelen och mest varit intresserad av mjukvara.
 * Vilket är en av anledningarna att jag sökte just programvaruteknik.
 *
 * @section Author Linus Ängeskog
 * Jag är 28 år och bor på Värmdö utanför Stockholm. Mitt stora intresse är musik och jag har en egen studio i Stockholm
 * där jag spelar in musik och programmerar ljusshower med mera, det är därifrån intresset för programmering kommer.
 * 
 *
 */



#include <iostream>
#include "GameOfLife.h"
#include "Support/MainArgumentsParser.h"

#ifdef DEBUG
#include <memstat.hpp>
#endif

using namespace std;

int main(int argc, char* argv[]) {

    MainArgumentsParser parser;
    ApplicationValues appValues = parser.runParser(argv, argc);

    if (appValues.runSimulation) {
        // Start simulation
        try {
            GameOfLife gameOfLife = GameOfLife(appValues.maxGenerations, appValues.evenRuleName, appValues.oddRuleName);
            gameOfLife.runSimulation();
        }
        catch(ios_base::failure &e){}

    }

    cout << endl;
    return 0;
}
